import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';

export default class Splash extends Component{
    render() {
        return (
          <View style={styles.wrapper}>
            <Text style={styles.title}>wwwwass</Text>
          </View>
        );
    }
}




const styles = StyleSheet.create({
  wrapper:{
    backgroundColor: 'white',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  title:{
    fontSize: 30,
    color: 'blue'
  }
});
